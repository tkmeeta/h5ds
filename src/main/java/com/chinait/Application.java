package com.chinait;

import java.util.List;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.util.DigestUtils;

import com.chinait.dao.AppUserRepository;
import com.chinait.domain.AppUser;
/**
     * 初始化用户
     * @author cyc
     * @param ctx
     */
/**
 * 权限 初始化角色  初始化用户
 * @author Administrator
 *
 */
@SpringBootApplication
public class Application {
    
    public static void main(String[] args) {
        ApplicationContext ctx = SpringApplication.run(Application.class, args);
        init(ctx);
        //ImprovedNamingStrategy
    }
    /**
     * 初始化用户
     * @author cyc
     * @param ctx
     */
    public static void init(ApplicationContext ctx){
    	AppUserRepository appUserRepository = ctx.getBean(AppUserRepository.class);
    	List<AppUser> list = (List<AppUser>) appUserRepository.findAll();
    	if(list.isEmpty()){
    		//初始化用户
    		AppUser user = new AppUser();
    		user.setLoginName("admin");
    		user.setUserName("cyc");
    		user.setPassword(DigestUtils.md5DigestAsHex("123456".getBytes()));
    		//初始化角色
    		//权限
    		user.setIsDelete(false);
    		appUserRepository.save(user);
    	}
    }
}