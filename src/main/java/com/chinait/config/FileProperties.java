package com.chinait.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "mtsee")
public class FileProperties {
	private String uploadFolder;
	private String uploadFolderUrlPrefix;
	private String uploadFileSize;
	public String getUploadFolder() {
		return uploadFolder;
	}
	public void setUploadFolder(String uploadFolder) {
		this.uploadFolder = uploadFolder;
	}
	public String getUploadFolderUrlPrefix() {
		return uploadFolderUrlPrefix;
	}
	public void setUploadFolderUrlPrefix(String uploadFolderUrlPrefix) {
		this.uploadFolderUrlPrefix = uploadFolderUrlPrefix;
	}
	public String getUploadFileSize() {
		return uploadFileSize;
	}
	public void setUploadFileSize(String uploadFileSize) {
		this.uploadFileSize = uploadFileSize;
	}
}
