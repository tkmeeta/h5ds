package com.chinait.service.impl;


/**
 * Spring Security权限管理
 * @author cyc
 * 
 */
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.chinait.config.UserInfo;
import com.chinait.dao.AppUserRepository;
import com.chinait.domain.AppUser;
import com.chinait.service.MyUserDetailsService;

@Service
public class UserDetailsSecurityServiceImpl implements MyUserDetailsService{
	@Autowired
	public AppUserRepository appUserRepository;

	@Override
	@Transactional
	public UserInfo loadUserByUsername(String userName)throws UsernameNotFoundException {
		
		AppUser appUser = appUserRepository.findByLoginNameAndIsDeleteIsFalse(userName);
		
		if (appUser == null) {
				throw new UsernameNotFoundException(userName + " 不存在！");
		}
		return new UserInfo(appUser,userName,appUser.getPassword(),null);
	}
}
