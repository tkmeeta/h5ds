package com.chinait.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HelloController {
	
    @RequestMapping("/")
    public String index() {
        return "test";
    }
    @RequestMapping("/zhuan")
    @ResponseBody
    public String zhuan(String t) {
    	System.out.println("test:"+t);
        return "test";
    }
    @RequestMapping("/ok")
    public String ok() {
        return "ok";
    }
}
